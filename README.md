# `rtthesis.typ`

This repository contains a re-implementation of the rtthesis LaTeX document
class, created by the Division of Automatic Control at ISY, Linköping
University for writing a master thesis.

## Package contents

- `rtthesis.typ` contains the package and some auxillary functions.
- `template.typ` is a minimal example document that uses the `rtthesis` template.
- `manual.typ` contains full documentation and usage manual, as well as a changelog.

## Getting started

For writing your own thesis, it is easiest to start with `template.typ`. Both
it and the documentation is built in GitLab CI and can be seen here:

- [manual.pdf](https://gitlab.com/sornas/rtthesis-typst/-/jobs/artifacts/main/browse?job=manual)
- [template.pdf](https://gitlab.com/sornas/rtthesis-typst/-/jobs/artifacts/main/browse?job=template)

### Fonts

The original LaTeX class uses the `KpRoman` font from the `kpfonts-otf` package.

To use it, first download the `kpfonts-otf` package from
https://mirrors.ctan.org/fonts/kpfonts-otf.zip and extract it to somewhere in
your font search path. For me, I extracted into `~/.fonts` so I had e.g.
`~/.fonts/kproman/KpRoman-Regular.otf`. You can verify that Typst finds this
font by running `typst fonts | grep KpRoman`.

Configure the template to use the font. I recommend the following configuration
(which is used in `template.typ`) since it looks functionally equivalent to the
LaTeX class, but I don't now if this is _required_ by ISY or not.

```typst
#show: rtthesis.with(
  ...
  font: (
    serif: "KpRoman",
    sans: "Liberation Sans",
  ),
)
```

## References

- Typst: https://typst.app/home
- Original rtthesis LaTeX documentclass: https://www.control.isy.liu.se/student/exjobb/liuthesis/liuthesis.html

## Development

The CI in this repository uses a custom Docker image that is pushed to the
GitLab crate registry. To update it, run:

```sh
docker build -t registry.gitlab.com/sornas/rtthesis-typst .
docker push registry.gitlab.com/sornas/rtthesis-typst
```
