#import "@preview/drafting:0.2.2"
#import "@preview/tidy:0.1.0"

#show link: it => { underline(offset: 2pt, it) }

#set page(margin: (x: 1in))

#align(center, text(size: 16pt, weight: "bold")[The `rtthesis` document template])
#align(center)[Gustav Sörnäs]
#align(center)[2025--02--28]

#set heading(numbering: "1.1")
#outline(indent: 0.5cm, depth: 2)

= Introduction

This is the user manual for the `rtthesis` document template. It is a
re-implementation of the `rtthesis` LaTeX document class created by numerous
people at Linköping University, Sweden and is intended to be usable for all
kinds of theses.

It is still early days, and there are probably more planned than implemented
features. Some larger planned features are described in @planned-features.

= Installation

`rtthesis` is not currently published as a Typst package. The recommended way of
getting started is either using a local installation of the Typst CLI or using
the Typst web app.

== Local Typst installation

1. Install the Typst CLI according to instructions at #link("https://github.com/typst/typst").
2. Clone or download the rtthesis-typst repository from #link("https://gitlab.com/sornas/rtthesis-typst").
3. Rename `template.typ` to a name of your choice, e.g. `thesis.typ`.
4. Optionally, remove `.gitlab-ci.yml`; or, if you're using GitLab, edit it to
  compile a ready PDF of your thesis every time you make a push to the repository.
  As it is written it's a good starting point.

My current recommendation, as of February 2025, is to write Typst-files in VS
Code using the `myriad-dreamin.tinymist` extension.
`tinymist` gives good LSP integration and an instant
split-screen preview of the current documentation, just like the web application.

== `typst.app` web app

1. Create an account on #link("https://typst.app").
2. Create a new project.
3. Upload the files `rtthesis.typ` and `template.typ` (possibly renamed to
  `thesis.typ`) as well as the `figures`-directory.

== Multiple authors

If you're writing your thesis together with someone else, either using a local
installation or using the web app will probably work. However, you should
probably try both and check what works best for you. With a local installation,
you can do a normal git workflow, or edit concurrently using for example VS Code
Live Share or some other editor integration for concurrent editing. (I would
recommend doing both.) The web app has very good multi-user support, although
when I've used it we sometimes ran into some unfortunate desynchronization
issues. But I assume it will only get better.

= Basic usage

`template.typ` contains a good starting point, but for completeness sake, here's
an example document that you can use to get started. You'll need to set some
parameters so the template can typeset the front matter.

#rect(width: 100%)[
  ```typst
  #import "rtthesis.typ": * // import everything from the package

  #show rtthesis.with(
     title: [Investigations of a Problem], // the title of your thesis
     subtitle: [An interesting subtitle],  // optional subtitle
     issn: [`LiTH-ISY-EX--YY/NNNN-SE`], // issn report number
     authors: ("Lisa Student"), // one author, or two:
     // authors: ("Lisa Student", "Linus Student"),
     supervisors: (("Doktorand Si", isy),       // supervisor with affiliation
                   ("Ingenjör Så", "Company")), // multiple supervisors possible
     examiner: (("Lena Lärare", isy)), // examiner with affiliation.
                                       // 'isy' and 'ida' are built-in
  )

  // And then you start writing!

  = Introduction

  According to all known laws of aviation,
  there is no way a bee should be able to fly.
  ```
]

Writing in Typst is very easy to pick up if you've ever used Markdown or
something like it. Everything you need can be found in the official Typst
documentation at #link("https://typst.app/docs").

// TODO: Thesis-specific Typst help?

= Available functions

This section contains generated documentation for some of `rtthesis.typ`. There
are more functions and variables that are primarly meant for internal use, but you can still
use them in your document if you want to. Just keep in mind that they might
change between versions.

#{
  set heading(numbering: none)
  let module = tidy.parse-module(read("rtthesis.typ"))
  tidy.show-module(module, style: tidy.styles.default, show-outline: false)
  set heading(numbering: "1.1")
}

= What's next?

Hopefully you know enough to get started writing your thesis. But the fun
doesn't stop here! Typst contains a package system and central package registry
where users can upload their packages for others to use. Here's a short list of
packages that might be useful for your thesis. The entire list can be found at
#link("https://typst.app/docs/universe/").

- *CeTZ* (#link("https://typst.app/universe/package/cetz")) -- Draw graphs and diagrams,
  comparable to TiKZ in LaTeX. Combine with *CeTZ-Plot* (#link("https://typst.app/universe/package/cetz-plot"))!

- *drafting* (#link("https://typst.app/universe/package/drafting")) -- Customizable reviewer notes, comments and TODOs rendered in the
  document. Further described in @sec:drafting.

- *in-dexter* (#link("https://typst.app/universe/package/in-dexter")) -- Create an index,
  usually put at the end of the thesis.

- *tablem* (#link("https://typst.app/universe/package/tablem/")) -- Markdown-like table syntax.
// Tables are described in @sec:tables.

== Reviewer notes using `drafting` <sec:drafting>

The `drafting` package can be used to insert margin notes or inline notes. It
needs to know about the current margins, so tell it about the S5 page margins.

#rect(width: 100%)[
  ```typst
  #import "@preview/drafting:0.2.2": *
  #import "rtthesis.typ": *

  Here's some text. #margin-note[And here's a note.]
  ```
]

Here's some text. #drafting.margin-note[And here's a note.]

#let margin-note = (it, ..args) => drafting.margin-note(..args, text(size: 8pt, it))

You'll notice that the margins in the default S5 page is very small. You might want to use a smaller text size: #margin-note(side: left)[This note has text with smaller size.]

```typst
#let margin-note = (it, ..args) => margin-note(..args, text(size: 8pt, it))
#margin-note(side: left)[This note has text with smaller size.]
```

One more useful feature for thesis work is color coding the notes. For example:
different areas, or different authors!

```typst
#let note-person-a = margin-note.with(stroke: blue)
#let note-person-b = margin-note.with(stroke: purple)
```

#let note-person-a = margin-note.with(stroke: blue)
#let note-person-b = margin-note.with(stroke: purple)

Here's some more text. #note-person-a[I think this is fine.] And even more.
#note-person-b[No, we need this too!]

Notes can be hidden by setting `#set-margin-note-defaults(hidden: true)`.

#drafting.inline-note[Finally, inline notes using `#inline-note` are also possible.]

There are some more features in `drafting`. Check out their example document at
#link("https://github.com/ntjess/typst-drafting"). (The above multiple-reviewer example
came straight from the documentation).

= Planned features <planned-features>

- Different layouts used by different institutions. The Department of Computer
  and Information Science (IDA) uses the `liuthesis` LaTeX document class which
  prints on A4 instead of S5. There is also no reason to not support modularising
  so you can design your own front matter, chapter style etc.

- Generating exhibit pages.

- _Optionally_ render a book-friendly PDF with recto/verso pages, alternating
  margins and a proper backpage.

- Include an externally typeset PDF as first page or entire front matter.

= Changelog

#set heading(numbering: none)

== 2025--02--28

Copy changes made during the TDDD89 course.

- Use `context` instead of `locate(loc => ...)`.
- Add `s5-on-a4` parameter.
- Add "list of listings".
- Add appendices.
- Make fonts configurable.
- Handle bibliography outside the template package.
- Set correctly supplements (chapter, appendix).
- Various minor fixes.

== 2024--01--02

Initial version.
